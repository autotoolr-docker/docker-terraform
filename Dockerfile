FROM alpine:latest

LABEL maintainer="Autotoolr <https://gitlab.com/autotoolr-docker>" cli_version="1.1.6"

RUN apk -v --update add ca-certificates && \
    apk add --virtual=build curl tar zip gzip && \
    curl -LO https://releases.hashicorp.com/terraform/1.1.6/terraform_1.1.6_linux_amd64.zip && \
    unzip terraform_1.1.6_linux_amd64.zip && \
    chmod +x ./terraform && \
    mv ./terraform /usr/local/bin/terraform && \
    apk --purge del build && \
    rm /var/cache/apk/*